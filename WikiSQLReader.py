import json
import jsonlines

###############################################################################

#method that prints the dataset
def dataset_printer(dataset_file_name, database_file_name):
    
    json_lines = open(dataset_file_name, 'r')
        
    for (json_index, json_line) in enumerate(json_lines):
        print(json_index)
        line=json.loads(json_line)
    
        print("-line: " + str(line))
        print("-phase: " + str(line['phase']))#phase: the phase in which the dataset was collection. We collected WikiSQL in two phases: 1 and 2.
        table_id = line['table_id']
        print("-table_id: " + str(table_id))#table_id: the ID of the table to which this question is addressed.
        print("-question: " + str(line['question']))#question: the natural language question written by the worker.
        print("-sql: " + str(line['sql']))#the SQL query corresponding to the question. This has the following subfields:
        select_column_index = line['sql']['sel']
        print("-sql select clause columns: " + str(select_column_index) + " " + str(convert_column_index_to_column_name(select_column_index, table_id, database_file_name)))#the numerical index of the column that is being selected. You can find the actual column from the table. It starts with 0.
        print("-sql select clause column aggregation operator: " + str(line['sql']['agg']) + " " + str(convert_aggregation_operator_index_to_aggregation_operator_name(line['sql']['agg'])))#the numerical index of the aggregation operator that is being used. I starts with 0. You can find the actual operator from Query.agg_ops in lib/query.py. There are 6 options: agg_ops = ['', 'MAX', 'MIN', 'COUNT', 'SUM', 'AVG']
        nr_triples = len(line['sql']['conds'])
        print("-sql where clause number of conditions (triples): " + str(nr_triples))
        
        for (i, cond_triple) in enumerate(line['sql']['conds']):
            print("--cond " + str(i))
            cond_column_index = line['sql']['conds'][i][0]
            print("---numerical index of the condition column: " + str(cond_column_index) + " " + str(convert_column_index_to_column_name(cond_column_index, table_id, database_file_name)))#column_index: the numerical index of the condition column that is being used. You can find the actual column from the table.
            print("---numerical index of the condition operator: " + str(line['sql']['conds'][i][1]) + " " + str(convert_operator_index_to_operator_name(line['sql']['conds'][i][1])))#operator_index: the numerical index of the condition operator that is being used. You can find the actual operator from Query.cond_ops in lib/query.py. There are 4 options cond_ops = ['=', '>', '<', 'OP']
            print("---comparison value, in either string or float type: " + str(line['sql']['conds'][i][2]))#condition: the comparison value for the condition, in either string or float type. 
    
        print()
    
    print("Number of lines " + "(" + dataset_file_name + "): " + str(json_index+1))
    
###############################################################################
    
#method that reads the dataset
def dataset_reader(dataset_file_name):
    
    json_lines = open(dataset_file_name, 'r')
    dataset_lines=[]
    
    
    
    for (json_index, json_line) in enumerate(json_lines):
        line=json.loads(json_line)
        dataset_lines.append(line)
    
    return dataset_lines

#def dataset_reader(dataset_file_name):
#    
#    dataset_lines=[]
#    
#    with jsonlines.open(dataset_file_name) as reader:
#        for obj in reader.iter(type=dict):
#            dataset_lines=[].append(obj)
#    reader.close()
#    return dataset_lines
    
###############################################################################
    
#Method that prints tables structures
def database_printer(database_file_name):
    json_lines = open(database_file_name, 'r')
    
    for (json_index, json_line) in enumerate(json_lines):
        line=json.loads(json_line)
        print("-table id: " + str(line['id']))
        #print("table name" + str(line['name']))#somes lines doesn't have page title
        #print("wikipedia table caption: " + str(line['caption']))#some lines doesn't have caption
        #print("wikipedia page title: " + str(line['page_title']))#somes lines doesn't have page title
        #print("wikipedia page section title: " + str(line['section_title']))#somes lines doesn't have page title
        print("-header: " + str(line['header']))
        print("-columns types: " + str(line['types']))
        rows=line['rows']
        print("-number of rows: " + str(len(rows)))
        print("-row example: " + str(line['rows'][0]))
        print()
        
    print("Number of tables " + "(" + database_file_name + "): " + str(json_index+1))
    
###############################################################################

#method that reads the database
def database_reader(database_file_name):
    
    json_lines = open(database_file_name, 'r')
    database_lines=[]
        
    for (json_index, json_line) in enumerate(json_lines):
        line=json.loads(json_line)
        database_lines.append(line)
    
    return database_lines

###############################################################################


#method that get the header of a table
def get_header(table_id, tables):
    header = ""
    
    #print("find tables: " + str(table_id))
    for t in tables:
        #print("t[id]: " + str(t['id']))
        if (t['id'] == table_id):
            header = t['header']
            break
    
    return header

###############################################################################

#Method that converts the column_index to the name of the columin
def convert_column_index_to_column_name(column_index, table_id, database_file_name):
    column_name=''
    
    tables = database_reader(database_file_name)
    #table = find_table(table_id, tables)
    #print("--------------TABLE---------------"+ str(table))
    #columns = table['header']
    columns = get_header(table_id, tables)
    column_name = columns[column_index]
    #print("==============column name: " + column_name)
    
    return column_name

###############################################################################

def convert_operator_index_to_operator_name(operator_index):
    cond_ops = ['=', '>', '<', 'OP']
    maxIndex=len(cond_ops)-1
    
    if((operator_index > maxIndex) or (operator_index < 0)):
        raise Exception("operator_index out of bounds: " + str(operator_index))
    
    operator_name = cond_ops[operator_index]
    return operator_name

###############################################################################
    
def convert_aggregation_operator_index_to_aggregation_operator_name(aggregation_operator_index):
    agg_ops = ['', 'MAX', 'MIN', 'COUNT', 'SUM', 'AVG']
    maxIndex=len(agg_ops)-1
    
    if((aggregation_operator_index > maxIndex) or (aggregation_operator_index < 0)):
        raise Exception("aggregation_operator_index out of bounds: " + str(aggregation_operator_index))
    
    aggregation_operator_name=agg_ops[aggregation_operator_index]
    
    return aggregation_operator_name

###############################################################################

#method that receives an array of dicts and write to a jsonl dataset file
def create_dataset_file(new_dataset_file_name, new_dataset):
    with jsonlines.open(new_dataset_file_name, mode='w') as writer:  
        writer.write_all(new_dataset)
    writer.close()

###############################################################################
## MAIN #######################################################################
###############################################################################

#dataset_file_name='dev.jsonl'
#dataset_file_name='train.jsonl'
dataset_file_name='test.jsonl'

#database_file_name='dev.tables.jsonl'
#database_file_name='train.tables.jsonl'
database_file_name='test.tables.jsonl'

#dataset_printer(dataset_file_name, database_file_name)
#database_printer(database_file_name)

#dataset = dataset_reader(dataset_file_name)
#print("dataset: " + str(len(dataset)))
#for item in dataset:
#    print(item)
    
#database = database_reader(database_file_name)
#print("database: " + str(len(database)))
#for item in database:
#    print(item)

###############################################################################
#read dataset file, create a new attribute called "nr_of_tables=1" for all 
#columns and create a new dataset file.
###############################################################################
#read dataset file
dataset = dataset_reader(dataset_file_name)

#create a new attribute called "nr_of_tables=1" for all columns
for item in dataset:
    item['nr_of_colmns']=1

new_dataset_file_name='new_dataset.jsonl'
create_dataset_file(new_dataset_file_name, dataset)

#read new_dataset file
new_dataset = dataset_reader(new_dataset_file_name)
#dataset_printer(new_dataset_file_name, database_file_name)
###############################################################################






























