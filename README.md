# WikiSQL Reader

Python code that reads WikiSQL (https://github.com/salesforce/WikiSQL) data available in JSONL format.